import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Simple Clock';
  isRunning = true;
  currentTime;

  startClock() {
    this.isRunning = true;
  }
  stopClock() {
    this.isRunning = false;
  }
  onTimechange(timeInMS: number) {
    this.currentTime = timeInMS;
    // console.log('received timechange event from child with value: ' + timeInMS );
  }
}
