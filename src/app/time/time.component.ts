import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-time',
  templateUrl: './time.component.html',
  styleUrls: ['./time.component.css']
})
export class TimeComponent implements OnInit {
  now;
  @Output() timechange = new EventEmitter<boolean>();
  @Input() running = true;
  constructor() { }

  ngOnInit() {
    this.updateTime();
  }
  updateTime() {
    if (this.running) {
      const ms: number = Date.now();
      const s: number = Math.floor(ms / 1000);
      const dif = ms - (s * 1000);
      this.now = ms;
      this.timechange.emit(this.now);
      setTimeout(() => this.updateTime(), (1000 - dif));
    } else {
      setTimeout(() => this.updateTime(), 1000);
    }
    // this clock method produces a result consistent with the system clock within just a few milliseconds
    // whether it runs or not is dependent on the running boolean set by the parent component.
  }
}
