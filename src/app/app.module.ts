import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { TimeComponent } from './time/time.component';
import { RetirementComponent } from './retirement/retirement.component';

@NgModule({
  declarations: [
    AppComponent,
    TimeComponent,
    RetirementComponent
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
