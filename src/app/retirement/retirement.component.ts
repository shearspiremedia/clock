import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-retirement',
  templateUrl: './retirement.component.html',
  styleUrls: ['./retirement.component.css']
})
export class RetirementComponent implements OnInit {
  @Input() nowtime;
  retirementDateString = '2025-11-01';
  retirementDate;
  retirementYears = 20;
  retirementDollars = 500000;
  earningsRate = 4;
  annualIncome = 0;
  socsecIncome = 22000;
  totalIncome = 0;
  secondsToGo = 0;
  daysToGo = 0;

  constructor() { }

  ngOnInit() {
    this.retirementDate = this.calculateRetirementDate();
    this.calculateIncome();
    this.getCountDown();
  }
  getCountDown() {
    const retirementDate = this.retirementDate;
    this.secondsToGo = Math.floor((retirementDate.getTime() - this.nowtime) / 1000);
    this.daysToGo = Math.floor(this.secondsToGo / (60 * 60 * 24));
    return this.secondsToGo;
  }
  calculateRetirementDate(){
    const [year, month, date] = this.retirementDateString.split('-').map(s => parseInt(s, 10));
    this.retirementDate = new Date(year, month - 1, date);

    return this.retirementDate;
  }

  calculateIncome(){
    const annualPrincipal = Math.round(this.retirementDollars / this.retirementYears);
    let totalEarnings = 0;
    const rate = this.earningsRate / 100;
    for (let i = 0; i < this.retirementYears; i++) {
      const newPrincipal = this.retirementDollars - (i * annualPrincipal);
      totalEarnings += (newPrincipal * rate);
    }
    this.annualIncome = annualPrincipal + Math.round(totalEarnings / this.retirementYears);
    this.totalIncome = this.annualIncome + this.socsecIncome;
  }

}
